# Bitbucket Pipelines Pipe: Azure Web Apps Containers Deploy

Deploys a container to [Azure Web Apps for Containers](https://azure.microsoft.com/en-us/services/app-service/containers/). Azure Web Apps for Containers enables you to easily deploy and run containerized applications on Windows and Linux without managing infrastructure. It offers auto-scaling and high availability.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: microsoft/azure-web-apps-containers-deploy:1.0.2
    variables:
      AZURE_APP_ID: '<string>'
      AZURE_PASSWORD: '<string>'
      AZURE_TENANT_ID: '<string>'
      AZURE_RESOURCE_GROUP: '<string>'
      AZURE_APP_NAME: '<string>'
      DOCKER_CUSTOM_IMAGE_NAME: '<string>'
      # DOCKER_REGISTRY_SERVER_URL: '<string>' # Optional.
      # DOCKER_REGISTRY_SERVER_USER: '<string>' # Optional.
      # DOCKER_REGISTRY_SERVER_PASSWORD: '<string>' # Optional.
      # SLOT: '<string>' # Optional.
      # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable                        | Usage                                                       |
| ------------------------------- | ----------------------------------------------------------- |
| AZURE_APP_ID (*)                | The app ID, URL or name associated with the service principal required for login. |
| AZURE_PASSWORD (*)              | Credentials like the service principal password, or path to certificate required for login. |
| AZURE_TENANT_ID  (*)            | The AAD tenant required for login with the service principal. |
| AZURE_RESOURCE_GROUP (*)        | Name of the resource group that the app service is deployed to.  |
| AZURE_APP_NAME (*)              | Name of the web app you want to deploy. |
| DOCKER_CUSTOM_IMAGE_NAME (*)    | The container custom image name and optionally the tag name.. |
| DOCKER_REGISTRY_SERVER_URL      | The container registry server url. |
| DOCKER_REGISTRY_SERVER_USER     | The container registry server username. |
| DOCKER_REGISTRY_SERVER_PASSWORD | The container registry server password. |
| SLOT                            | Name of the slot. Defaults to the production slot if not specified. |
| DEBUG                           | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

You will need to configure required Azure resources before running the pipe. The easiest way to do it is by using the Azure cli. You can either [install the Azure cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) on your local machine, or you can use the [Azure Cloud Shell](https://docs.microsoft.com/en-us/azure/cloud-shell/overview) provided by the Azure Portal in a browser.

### Service principal

You will need a service principal with sufficient access to create or an Azure App Service, or update an existing App Service. To create a service principal using the Azure CLI, execute the following command in a bash shell:

```sh
az ad sp create-for-rbac --name MyServicePrincipal
```

Refer to the following documentation for more detail:

* [Create an Azure service principal with Azure CLI](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli)

### Azure App Service app

Using the service principal credentials obtained in the previous step, you can use the following commands to create an Azure App Service instance in a bash shell:

```bash
az login --service-principal --username ${AZURE_APP_ID}  --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}

az group create --name ${AZURE_RESOURCE_GROUP} --location australiaeast

az appservice plan create --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --sku B1 --is-linux

az webapp create --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --plan $AZURE_APP_NAME

az webapp config appsettings set --resource-group ${AZURE_RESOURCE_GROUP} --name ${AZURE_APP_NAME} --settings WEBSITES_PORT=${WEBSITES_PORT}
```

Refer to the following documentation for more detail:

* [Use a custom Docker image for Web App for Containers](https://docs.microsoft.com/en-us/azure/app-service/containers/tutorial-custom-docker-image)

## Examples

### Basic example

```yaml
script:
  - pipe: microsoft/azure-web-apps-containers-deploy:1.0.2
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
      AZURE_APP_NAME: $AZURE_APP_NAME
      DOCKER_CUSTOM_IMAGE_NAME: $DOCKER_CUSTOM_IMAGE_NAME
```

### Advanced example

```yaml
script:
  - pipe: microsoft/azure-web-apps-containers-deploy:1.0.2
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
      AZURE_APP_NAME: $AZURE_APP_NAME
      DOCKER_CUSTOM_IMAGE_NAME: $DOCKER_CUSTOM_IMAGE_NAME
      DOCKER_REGISTRY_SERVER_URL: $DOCKER_REGISTRY_SERVER_URL
      DOCKER_REGISTRY_SERVER_USER: $DOCKER_REGISTRY_SERVER_USER
      DOCKER_REGISTRY_SERVER_PASSWORD: $DOCKER_REGISTRY_SERVER_PASSWORD
      SLOT: 'staging'
      DEBUG: 'true'
```

## Support

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance. To discuss this sample with other users, please visit the Azure DevOps Services section of the Microsoft Developer Community: https://developercommunity.visualstudio.com/spaces/21/index.html.
